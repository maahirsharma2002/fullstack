First clone then Run

mvn install

docker build netmeds360-api .

then

docker run --name mysql-standalone -e MYSQL_ROOT_PASSWORD=password -e MYSQL_DATABASE=users -e MYSQL_USER=sa -e MYSQL_PASSWORD=password -d mysql:5.7

docker run -d -p 8089:8089 --name netmeds360-api --link mysql-standalone:mysql netmeds360-api

