package api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;



@RestController
@CrossOrigin
@RequestMapping("/")
public class ServiceController {
	@Autowired
	ServiceRepository serviceRepo;
	
	@GetMapping("/services")
	public ResponseEntity<List<ServiceModel>> FindAll(){
		return ResponseEntity.status(200).body(serviceRepo.findAll());
	}
}