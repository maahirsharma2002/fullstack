package api.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin
public class userController {
    @Autowired
    userRepo userRepo;

    @RequestMapping("/")
    public ResponseEntity<HashMap<String, Object>> check () {
    	HashMap<String, Object> map = new HashMap<>();
    	map.put("message", "Success");
        return ResponseEntity.status(200).body(map);
    }
    
    @PostMapping("/users/register")
    public ResponseEntity<Object> registeruser(@Valid @RequestBody userModel user) {
    	HashMap<String, Object> map = new HashMap<>();
        if(user.getUsername() == null || user.getPassword()==null) {
        	map.put("message", "Wrong Parameters");
        	return ResponseEntity.status(300).body(map);
        }
        List<userModel> newUser = userRepo.findByUsername(user.getUsername());
        if(newUser.size()>0) {
            map.put("message", "Already Exists");
        	return ResponseEntity.status(400).body(map);
        }
        userRepo.save(user);
        return ResponseEntity.status(200).body(user);        		
    }
    
    @PostMapping("/users/login")
    public ResponseEntity<Object> loginuser(@Valid @RequestBody userModel user) {
    	HashMap<String, Object> map = new HashMap<>();
    	if(user.getUsername() == null || user.getPassword()==null) {
    		map.put("message", "Wrong Parameters");
        	return ResponseEntity.status(300).body(map);
        }
    	List<userModel> newUser = userRepo.findByUsername(user.getUsername());
    	if(newUser.size() == 0) {
    		map.put("message", "not authorized");
    		return ResponseEntity.status(404).body(map);
    	}
    	if(user.equals(newUser.get(0))) {
    		user.setLoggedIn(true);
    		return ResponseEntity.status(200).body(user);
    	}
    	map.put("message", "not authorized");
        return ResponseEntity.status(404).body(map);
    }
    
    @PostMapping("/users/logout")
    public ResponseEntity<Object> loguserOut(@Valid @RequestBody userModel user) {
    	HashMap<String, Object> map = new HashMap<>();
    	if(user.getUsername() == null || user.getPassword()==null) {
    		map.put("message", "Wrong Parameters");
        	return ResponseEntity.status(300).body(map);
        }
    	List<userModel> newUser = userRepo.findByUsername(user.getUsername());
    	if(newUser.size() == 0) {
    		map.put("message", "not authorized");
    		return ResponseEntity.status(404).body(map);
    	}
    	if(user.equals(newUser.get(0))) {
    		user.setLoggedIn(false);
    		map.put("message", "Success");
    		return ResponseEntity.status(200).body(map);
    	}
    	map.put("message", "not authorized");
        return ResponseEntity.status(404).body(map);
    }
    @DeleteMapping("/users/all")
    public ResponseEntity<Object> deleteusers() {
        userRepo.deleteAll();
        return ResponseEntity.status(200).body(null);
    }
}
