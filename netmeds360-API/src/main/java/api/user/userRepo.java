package api.user;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface userRepo extends JpaRepository<userModel, Long> {
	
	List<userModel> findByUsername(String username);

	
}
