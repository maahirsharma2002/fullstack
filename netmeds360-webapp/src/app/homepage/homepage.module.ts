import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomepageComponent } from './homepage.component';
import { NavbarComponent } from './navbar/navbar.component';
import { ServiceRenderComponent } from './service-render/service-render.component';
import { ServiceListComponent } from './service-list/service-list.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatToolbarModule} from '@angular/material/toolbar';


@NgModule({
  declarations: [
    HomepageComponent,
    NavbarComponent,
    ServiceRenderComponent,
    ServiceListComponent
  ],
  imports: [
    CommonModule,
    MatSidenavModule,
    MatToolbarModule
  ],
  exports: [
    HomepageComponent
  ]
})
export class HomepageModule {}
