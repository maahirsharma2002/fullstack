import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ServiceRenderComponent } from './service-render.component';

describe('ServiceRenderComponent', () => {
  let component: ServiceRenderComponent;
  let fixture: ComponentFixture<ServiceRenderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ServiceRenderComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ServiceRenderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
